/**
 * Retrieve the value from a given cell in a given sheet
 * 
 * @param {number}
 *            Row of cell
 * @param {number}
 *            Column of cell
 * @param {Sheet}
 *            Sheet from which to retrieve data
 * @return {string} Data in the specified cell or undefined if out of bounds
 */
function getValue(row, column, sheet) {
	var range = sheet.getDataRange();
	var data = range.getValues();

	if (range.getNumRows() < row + 1) {
		return undefined;
	} else if (range.getNumColumns() < column + 1) {
		return undefined;
	} else {
		return data[row][column];
	}
}

/**
 * Set the value off a given cell in a given sheet
 * 
 * @param {number}
 *            Row of cell
 * @param {number}
 *            Column of cell
 * @param {Sheet}
 *            Sheet from which to retrieve data
 * @param {string}
 *            Value to be set in cell
 */
function setValue(row, column, sheet, value) {
	if (sheet.rows < row + 1) {
		var rij = [];
		rij.push(value);
		sheet.appendRow(rij);
	} else {
		var cell = sheet.getRange('a1');
		cell.offset(row, column).setValue(value);
	}
}

/**
 * Set the value off a given cell in a given sheet and give the cell a
 * background color
 * 
 * @param {number}
 *            Row of cell
 * @param {number}
 *            Column of cell
 * @param {Sheet}
 *            Sheet from which to retrieve data
 * @param {string}
 *            Value to be set in cell
 * @param {string}
 *            Background color to be set
 */
function setValueColor(row, column, sheet, value, backgroundcolor) {
	if (sheet.rows < row + 1) {
		var rij = [];
		rij.push(value);
		sheet.appendRow(rij);
	} else {
		var cell = sheet.getRange('a1');
		cell.offset(row, column).setValue(value);
		cell.offset(row, column).setBackground(backgroundcolor);
	}
}

/**
 * Set the value off a given cell in a given sheet, give the cell a background
 * color and give the text a color
 * 
 * @param {number}
 *            Row of cell
 * @param {number}
 *            Column of cell
 * @param {Sheet}
 *            Sheet from which to retrieve data
 * @param {string}
 *            Value to be set in cell
 * @param {string}
 *            Background color to be set
 * @param {string}
 *            Text color to be set
 */
function setValueFullColor(row, column, sheet, value, backgroundcolor,
		fontcolor) {
	if (sheet.rows < row + 1) {
		var rij = [];
		rij.push(value);
		sheet.appendRow(rij);
	} else {
		var cell = sheet.getRange('a1');
		cell.offset(row, column).setValue(value);
		cell.offset(row, column).setBackground(backgroundcolor);
		cell.offset(row, column).setFontColor(fontcolor);
	}
}
